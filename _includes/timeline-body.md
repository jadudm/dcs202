
<div id='timeline-embed' style="width: 100%; height: 400px"></div>


<script>
  function colorByGroup(group) {
    var colors = {
      "Reading"   : "#004435", 
      "Writing"   : "#707170",
      "Code"      : "#540030", // "#FFD700", // "#A000FF",
      "In Class"  : "#881124",
			"Plus One"  : "#FFCE33",
			"Fun"       : "#640A99"
    };
    
    return { color: colors[group] };
  }
</script>

<script>
  
  var term_start = moment("{{site.term_start}}");  
  var ndx = 0;
  var objects = [];
  var KJE = [];
  var ndx = 0;
  var start = 0;
  var dobj = {};
  var txt = "";
  var dmom = {};
  var start_slide = 0;
  
  {% for ass in site.assignments %}
  
    // Moments are mutable... have to reset it every time through.
    // Every "add" call modifies the moment.
    ndx = {{forloop.index0}};
  
    dmom        = moment("{{site.term_start}}");
    dmom.add({{ass.week}}, 'weeks').add({{ass.day}}, 'days');
    
    dobj = {};
    dobj.year           = dmom.format("YYYY");
    dobj.month          = dmom.format("MM");
    dobj.day            = dmom.format("DD");
    
    
    dobj.hour           = "{{site.class_start_hour}}";
    dobj.minute         = "{{site.class_start_minute}}";
    
    var hour_padded     = "";
    if ({{site.class_start_hour}} < 10) {
      hour_padded = "0{{site.class_start_hour}}";
      } else { hour_padded = "{{site.class_start_hour}}";
    }
    var time_to_parse       = "{{site.term_start}}".replace(/-/g, "") + "T" + hour_padded + "{{site.class_start_minute}}";
    var display_time    = (moment(time_to_parse)
      .add({{ass.week}}, 'weeks')
      .add({{ass.day}}, 'days')
      .format("dddd, MMM Do HH:mm"));
    
    // console.log("Parse: " + time_to_parse);  
    // console.log("Parsed: " + display_time);
    
    dobj.display_date   = display_time;
    
    
    // If this assignment is before now...
    if (dmom.isBefore(moment())) {

      start_slide += 1;
      console.log("Starting at index... " + start_slide);
    }
    
    KJE[ndx] = {};
    KJE[ndx].start_date = dobj;
    KJE[ndx].group = "{{ass.type}}";

    // console.log("Color: " + KJE[ndx].group);
    // console.log(colorByGroup(KJE[ndx].group));
    KJE[ndx].background = colorByGroup(KJE[ndx].group);
    KJE[ndx].text = { headline: "{{ass.title}}", text: "{{ass.slug}}<br><em><a href='{{ass.url | prepend: site.baseurl }}.html'>read more...</a></em>"};
   
    $("#due{{forloop.index0}}").html((moment("{{site.term_start}}")
      .add({{ass.week}}, 'weeks')
      .add({{ass.day}}, 'days')
      .format("dddd, MMM Do")));
      
    $("#submit{{forloop.index0}}").attr("href", ("https://docs.google.com/forms/d/e/1FAIpQLSfP3XzK6WDypkNVw8X_NiuZYFg3fOrooPfoaxhHGZt37AvNRA/viewform?usp=pp_url&"
      + "entry.37685363=" 
      + "{{ass.title}}"
      + "&entry.1208641331="
      + (moment("{{site.term_start}}")
        .add({{ass.week}}, 'weeks')
        .add({{ass.day}}, 'days')
        .format("YYYY-MM-DD"))
      ));
  {% endfor %}
  
  KJ = {};
  KJ.events = KJE;
  
  // I walked off the end of the list...
  var timeline_json = KJ;
  console.log("timeline length: " + timeline_json.events.length);
  console.log("start_slide: " + start_slide);
  if (start_slide >= timeline_json.events.length) {
    start_slide = timeline_json.events.length - 1;
  }
  
  var options = {
    start_at_slide: start_slide,
    timenav_position: "top",
    // Attempting to zoom out...
    scale_factor: 0.5,
    initial_zoom: 0.5
  }
  window.timeline = new TL.Timeline('timeline-embed', timeline_json, options);
</script>