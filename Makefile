all:
	@echo "'make upload' to build and upload the site."
	
buildjadud:
	jekyll build --baseurl /teaching/dcs202w18
	
build:
	jekyll build --baseurl /courses/dcs202w18
	
# echo lynxrufus@lynxruf.us:/home/jadudm/jadud.com/teaching/dcs102w18

uploadjadud: buildjadud
	scp -r _site/* jadudm@jadud.com:~/jadud.com/teaching/dcs202w18/

# scp -r _site/* lynxrufus@lynxruf.us:~/lynxruf.us/courses/dcs102w18/
upload: build
	rsync -avr -e "ssh" \
		--delete-after --delete-excluded \
		_site/ \
		lynxrufus@lynxruf.us:/home/lynxrufus/lynxruf.us/courses/dcs202w18/