import unittest
from commonsfood import CommonsFood

class TestCommonsFood (unittest.TestCase):

    def test_setRotationFrequency (self):
        cf = CommonsFood("Carrots")
        cf.setRotationFrequency(7)
        self.assertEqual(7, cf.rotationFrequency)
        
    def test_addRestriction (self):
        cf = CommonsFood("Carrots")
        cf.addRestriction("vegan")
        
        if "vegan" in cf.restrictions:
            self.assertTrue(True)
        else:
            self.assertTrue(False)
        
    def test_timesPerMonth1 (self):
        cf = CommonsFood("Carrots")
        cf.setRotationFrequency(7)
        self.assertEqual(4, cf.timesPerMonth(30))

    def test_timesPerMonth2 (self):
        cf = CommonsFood("Apples")
        cf.setRotationFrequency(1)
        self.assertEqual(30, cf.timesPerMonth(30))
    
    def test_hasRestriction1 (self):
        cf = CommonsFood("Apples")
        cf.addRestriction("vegan")
        self.assertTrue(cf.hasRestriction("vegan"))
        
    def test_hasRestriction2 (self):
        cf = CommonsFood("Apples")
        cf.addRestriction("vegan")
        self.assertFalse(cf.hasRestriction("meat"))

if __name__ == '__main__':
    unittest.main()