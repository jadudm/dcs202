---
week:   "2"
day:    "1"
title:  "Practice, Practice, Practice"
type:   "Code"
slug:   "Amazing A+ Practice"
upload: true
rubric: [ logic, clarity ]
---

From our in-class practice, I want to first share what your code should probably come out looking like. If not this week, then you should be working towards it.

```python
# CONTRACT
# fizzbuzz : number -> string
# PURPOSE
# Plays the game fizzbuzz. 
# - Multiples of 3 are "fizz"
# - Multiples of 7 are "buzz"
# - Multiples of both are "fizzbuzz"
def fizzbuzz (num):
  mod3 = (num % 3)
  mod7 = (num % 7)
  if mod3 == 0 and mod7 == 0:
  elif mod3 == 0:
    return "fizz"
  elif mod7 == 0:
    return "buzz"

# CHECKS
print("fizzbuzz() checks.")
print("fizzbuzz(0) is " + fizzbuzz(0))
print("fizzbuzz(3) is " + fizzbuzz(3))
print("fizzbuzz(7) is " + fizzbuzz(7))
print("fizzbuzz(21) is " + fizzbuzz(21))
```

This code has a clear contract, indicating 

* the name of the function, 
* the **type(s)** of the input(s) to the function,
* the **type** of the return value of the function, and 
* Some code that will check to see if the function is doing what you expect.

It is common to resist writing functions and tests. Failure to do so is a path to confusion. Begin establishing good, healthy programming habits now; it will pay dividends when you are trying to do more complex things.

## Practice

Here are some more problems you can use to warm up to working with lists. The general pattern is that I'm asking you to write isolated functions that consume one or more arguments, and return a value.

* When the campus squirrels get together to plan their next attack on the college dormitories, they like to do so wearing hats stolen from students. A planning session is successful when the number of hats is equal to or greater than the number of squirrels. Write a function called **squirrelAttack** that consumes the number of squirrels and the number of hats, and returns True when the success condition is met, and False otherwise.

* You've been caught speeding; we'll call this function **caughtSpeeding**. The officer runs an algorithm to give you a ticket. If you are 2-4 miles over the limit (inclusive) you get a "warning," which is a 0 dollar ticket. If you are 5-9 miles per hour over (inclusive), you get an 80 dollar ticket. If you are 10-15 miles over (inclusive), you get a $200 ticket. If you are doing more than 15 miles over the limit, you get a 500 fine, and have to go to court. Your function should take a speed limit (in mph), the speed you are traveling, and return the cost of your infraction. (A [CodingBat](http://codingbat.com/prob/p137202) question.)

* Given a number, return a random number between 1 and that number, inclusive. We'll call this **dieRoll**.

* Given a two numbers, return True if the first is a divisor of the second. We'll call this **checkDivisor**.

* Let's write the function **alarmClock**. Given a day of the week encoded as 0=Sun, 1=Mon, 2=Tue, ...6=Sat, and a boolean indicating if we are on vacation, return a string of the form "7:00" indicating when the alarm clock should ring. Weekdays, the alarm should be "7:00" and on the weekend it should be "10:00". Unless we are on vacation -- then on weekdays it should be "10:00" and weekends it should be "off". (This is [this problem from CodingBat](http://codingbat.com/prob/p119867).)

For each of these problems, you should write a complete contract as well as set of checks to confirm that you've written code that does what you expect it to.

## More Practice

The app [Py looks promising](https://www.getpy.com/). It involves fill-in-the-blank and multiple choice questions, which emphasize reading and thinking. This could be a very nice complement to the act of writing code.

