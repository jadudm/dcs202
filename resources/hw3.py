import copy
import sys

sys.setrecursionlimit(100000)

def strip(ls):
    return ls.strip()

#CONTRACT
# first : list -> element
# PURPOSE
# Consumes a list, and returns the first element of that list.
# NOTE
# This will fail on the empty list.
def first (ls):
	return ls[0]

# CONTRACT
# rest : list -> list
# PURPOSE
# Consumes a list, and returns the list that is made
# up of everything *except* the first element. So...
# the rest of the list.
# NOTE
# This will fail on the empty list.
def rest (ls):
	return ls[1:]

# CONTRACT
# isempty : list -> boolean
# PURPOSE
# Consumes a list, and returns true if the list
# is the empty list. Returns false if the list 
# contains one or more elements.
def isempty(ls):
    return ls == []

# CONTRACT
# string2list : string -> list-of characters
# PURPOSE
# Takes a string and returns a list of characters.
def string2list(string):
    list(string)

# CONTRACT
# cons : thing list -> list
# PURPOSE
# Takes a thing and puts it on the front of a list.
def cons(o, ls):
    if ls == []:
        return [o]
    else:
        ls2 = copy.deepcopy(ls)
        ls2.insert(0, o)
        return ls2

def stringToWords (a_string):
  return a_string.split(" ")
  
def slurpFile (filename):
    fp = open(filename)
    return fp.readlines()

posls = slurpFile("positive-words.txt")
negls = slurpFile("negative-words.txt")


def stripList (los):
    if isempty(los):
        return []
    else:
        return cons(strip(first(los)), stripList(rest(los)))

def splitSentences (los):
    if isempty(los):
        return []
    else:
        return cons(stripList(stringToWords(first(los))),
                    splitSentences(rest(los)))

# test = stringToWords("Hello there everyone")
# print(first(test))
# print(rest(test))
print(splitSentences(posls))

