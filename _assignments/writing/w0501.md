---
week:   "5"
day:    "1"
title:  "Exploring Dictionaries"
type:   "Code"
slug:   "Like hash tables... but, hash tables"
upload: true
rubric: [ logic, clarity ]
---

We're now starting to explore additional data structures. That suggests we have data.

First, I've linked the code that I demonstrated in class. That may be useful as a reference.

Second, you may find the chapter on dictionaries in Downey useful. Give it another read, now that you've had time to see and think about dictionaries.

Then, lets try putting them to use.

---

The [CORGIS collection](https://think.cs.vt.edu/corgis/python/index.html) at Virginia Tech could be useful for your data exploration project. It is a critical part of this homework.

Let's talk [SATs](https://think.cs.vt.edu/corgis/python/school_scores/school_scores.html). To grab the data, in your terminal, create a new folder, cd into it, and then get the code and data.

    mkdir corgis
    cd corgis
    curl -O https://think.cs.vt.edu/corgis/python/school_scores/school_scores.py
    curl -O https://think.cs.vt.edu/corgis/python/school_scores/school_scores.db

If you [visit the page for this dataset](https://think.cs.vt.edu/corgis/python/school_scores/school_scores.html), you'll find a button that says "explore schoo_scores data." Click that button.

The button shows you that the data is a list that contains zero or more dictionaries. That's what that interface *means*. Then, you can click the dictionary. It will expand, and the expanded view tells you that each dictionary has 8 keys: "Gender", "Year", "GPA", "State", "Academic Subjects", "Family Income", "Total", and "Score Ranges".

So, if you loop through the list of dictionaries, each one will have those keys. This might look like:

```python
import school_scores
scores = school_scores.get_all()

for score in scores:
    print(score["Gender"])
```

should print the gender of each person in the database.

If you wanted to only print the first ten, you might:

```python
import school_scores
scores = school_scores.get_all()

for ndx in range(0, 10):
    score = scores[ndx]
    print(score["Gender"])
```

Note: indexing into a list, and pulling a key from a dictionary look *really, really similar*. Like, the same. Can you point out which is which?

## Question 1
 
With a partner, discuss what kinds of questions you can ask. Think in terms of "counting" questions. How many people in this dataset identify as male or female? (I suspect the SAT only provides two choices.) How many subjects have a GPA of "A"? By gender? How many subjects are there in any given state?

Write out how you would write the code for these; that is, plan the code you would write to implement these solutions. Write a clear plan for at least three. 

Implement these.

## Question 2

What is the average family income of all people in this dataset who are from Maine? Alabama? California?

To answer this, you're going to have to look at the "Family Income" key, and think about how you would get a count of the number of people in each category for each state. That is, how many people are in the "Less than 20k" category in the state of Maine?

Once you think about how to *extract* the data, you'll need to think about *storing* the data you've found. This may involve creating a new dictionary. For example, you could have one called "stateIncomes," and it could be structured as:

    state : dictionary
  
that is, each key would be a state ("AL", "ME", etc.), and each dictionary would hold the keys for the income levels. You could then add up all of the people you find in each category.

Hm.

Tricky.

Wrestle with it, and ask questions.

## Question 3

What is the average Math SAT score for people who come from low-income brackets? High-income brackets? 

How would you present this information graphically? 

---

These are challenging questions. I'll happily explore them with you in-class, but spend some time in the next few days looking at these questions. It may be too much too soon, but we'll see. 

Apologies for the late release. Dive in, ask questions by email before class, and I'm happy to try and get together with you this weekend.




## Question 3

