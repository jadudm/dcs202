import unittest
from laptop import Laptop

class TestLaptop (unittest.TestCase):
    def setupMac(self):
        lappy = Laptop("Apple", "Macbook")
        lappy.setCost(1300)
        lappy.setWifi(True)
        lappy.setWeight(920)
        lappy.setRAM(8)
        lappy.setStorage(256)
        return lappy
    
    def setupChromebook(self):
        lappy = Laptop("Google", "Chromebook")
        lappy.setCost(200)
        lappy.setWifi(True)
        lappy.setWeight(800)
        lappy.setRAM(2)
        lappy.setStorage(16)
        return lappy
        
    def test_setCost (self):
        lappy = Laptop("Apple", "Macbook")
        lappy.setCost(1300)        
        self.assertEqual(1300, lappy.cost)
    
    def test_setWifi (self):
        lappy = Laptop("Apple", "Macbook")
        lappy.setWifi(True)        
        self.assertEqual(True, lappy.wifi)
    
    def test_setWeight (self):
        lappy = Laptop("Apple", "Macbook")
        lappy.setWeight(920)        
        self.assertEqual(920, lappy.weight)
    
    def test_setRAM (self):
        lappy = Laptop("Apple", "Macbook")
        lappy.setRAM(8)        
        self.assertEqual(8, lappy.RAM)

    def test_setStorage (self):
        lappy = Laptop("Apple", "Macbook")
        lappy.setStorage(256)        
        self.assertEqual(256, lappy.storage)
    
    def test_iAmMoreExpensive1 (self):
        mac = setupMac()
        chromebook = setupChromebook()
        self.assertTrue(mac.iAmMoreExpensive(chromebook))
        
    def test_iAmMoreExpensive2 (self):
        mac = setupMac()
        chromebook = setupChromebook()
        self.assertFalse(chromebook.iAmMoreExpensive(mac))
    
    def test_weightInPounds(self):
        lappy = Laptop("Random", "Example")
        lappy.setWeight(1000)
        weight = lappy.weightInPounds()
        inRange = (weight >= 2.19) and (weight <= 2.21)
        self.assertTrue(inRange)
        
if __name__ == '__main__':
    unittest.main()