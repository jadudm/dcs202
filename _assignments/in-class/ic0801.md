---
week:   "8"
day:    "1"
title:  "Revisiting Sentiment Analysis"
type:   "In Class"
slug:   "Classes Continued"
upload: false
---

Everyone loves [sentiment analysis](http://lynxruf.us/courses/dcs202w18/a/writing/w0301.html).

We're going to circle back around to code we've seen previously. And, in doing so, see how we can simplify our code with classes and objects.

## Lists of Lists... Oh My!

When we were working on the sentiment analysis, we only had strings and lists. It was rough. Now, we have more tools, like dictionaries, and classes.

Lets see what we can do.

## The Class

We were working with the idea that we had a file containing sentences (tweets), one sentence per line. What if we turned each of those sentences into an object? That would mean we would have a class for tweets.

<pre><code class="language-python">import string

# Helpers
def cleanup (lostrings):
    templist = [] 
    for aString in lostrings:
        templist.append(aString.lower().strip())
    return templist
 
def slurp (file):
    lolines = []
    fp = open(file)
    for line in fp:
        lolines.append(line)
    return lolines
    
# You'll need to pull in the wordlists.
posls  = cleanup(slurp("positive-words.txt"))
negls  = cleanup(slurp("negative-words.txt"))
tweets = cleanup(slurp("cleantweets.txt"))

class Tweet (object):
    pass
</code></pre>

## The Fields

Now, what variables do we need in the class? What would represent a tweet? This is where we should think about all the ways we mangled and mogrified the tweet. For example, we had the sentence version, and the split version, and the version where we scored each word...

So, lets think about those fields. I'm guessing we could have:

* originalTweet (string)
* tweetSplit (list of strings)
* scoredWords (dictionary of strings)
* score (number)

The first field will store the original tweet. The second field will store the same thing, but it will be the tweet split, so it is a list of words. The final field, <code>wordsDict</code>, will be a dictionary where each key is a word, and each value is a -1, 0, or +1, depending on whether it is a negative, neutral, or positive word. The score will be the final, summed score.

These might look like:

<pre><code class="language-python">    originalTweet  = ""
    tweetSplit     = None
    scoredWords    = None
    score          = 0
</code></pre>

## Init

We need to create an init function. It should take a tweet (or sentence) as an argument

<pre><code class="language-python">class Tweet (object):
def __init__(self, aTweet):
    self.originalTweet = aTweet
</code></pre>

## Split and Cleanup

Now, we need a method that will split the original tweet, clean it up, and store the result in the field <code>tweetSplit</code>

<pre><code class="language-python">def splitAndCleanup (self):
    listOfWords = self.originalTweet.split(" ")
    templist = []
    for aString in listOfWords:
        templist.append(aString.lower().strip())

    self.tweetSplit = []
    for aString in templist:
        self.tweetSplit.append(''.join(ch for ch in aString if ch.isalnum()))
</code></pre>

## Score!

<pre><code class="language-python">def scoreWords (self):
    global posls, negls
    self.scoredWords = {}
    for aWord in self.tweetSplit:
        if aWord in posls:
            self.scoredWords[aWord] = 1
        elif aWord in negls:
            self.scoredWords[aWord] = -1
        else:
            self.scoredWords[aWord] = 0
</code></pre>

## Score Encore!

Actually, we have to sum things up.

<pre><code class="language-python">def finalScore(self):
    sum = 0
    for word in self.scoredWords:
        sum = sum + self.scoredWords[word]
    self.score = sum
</code></pre>        

## Where are we at?

Let's try it. Outside of the class... grab the tweets, and try 

<pre><code class="language-python">
  allTweets = []
  for line in tweets:
      tweet = Tweet(line)
      tweet.splitAndCleanup()
      tweet.scoreWords()
      tweet.finalScore()
      allTweets.append(tweet)
   
   for tweet in allTweets:
      print(str(tweet.score) + " - " + tweet.originalTweet)
</code></pre>

# Did It Work?

If not, lets figure it out. It's close. I never ran this code, so it's probably full of cheese. Or holes. Or full of holes like cheese.

To test it, go into your terminal, and run <code>python sentiment.py</code>, assuming you saved all your code in a file called <code>sentiment.py</code>.

# Extensions

Now, we have a list of all tweets. Each has a score. How could we:

* Find the most positive tweet?
* Find the least positive tweet?
* Find all neutral tweets?
* Find the shortest tweet?
* Find the longest tweet?

Note that these are operations on a list of tweets. That is, you would be writing loops over the <code>allTweets</code> list to find each of those things. 

We don't have a class representing a list of tweets. We could create one, and bury all of this stuff in another class. If it mattered to us, we could do it.
