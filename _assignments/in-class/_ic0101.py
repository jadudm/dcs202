def first (ls):
	return ls[0]

def rest (ls):
	return ls[1:]

def isempty(ls):
    return ls == []

def string2list(string):
    list(string)

l1 = [ 1, 2, 3, 4, 5 ]
l2 = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]

print(first(l1))
print(rest(l1))

def listsum (ls):
    if isempty(ls):
        return 0
    else:
        return first(ls) + listsum(rest(ls))

print(listsum(l1))
print(listsum(l2))

print