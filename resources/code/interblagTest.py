import unittest
from interblag import Interblag

class TestInterblag (unittest.TestCase):
    def test_timeSpentInApp (self):
        ib = Interblag("Feedly")
        ib.setTimesPerDayOnApp(5)
        total = ib.timeSpentInApp(3)
        self.assertEqual(total, 15)

    def test_notSameApp (self):
        ib1 = Interblag("Tweeter")
        ib2 = Interblag("Feedly")
        self.assertFalse(ib1.sameApp(ib2)) 

    def test_isSameApp (self):
        ib1 = Interblag("Tweeter")
        ib2 = Interblag("Tweeter")
        self.assertTrue(ib1.sameApp(ib2)) 
    
    def test_setTimes (self):
        ib = Interblag("Feedly")
        ib.setTimesPerDayOnApp(30)
        self.assertEquals(ib.timesPerDayOnApp, 30)
        
if __name__ == '__main__':
    unittest.main()