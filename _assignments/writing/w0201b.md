---
week:   "2"
day:    "1"
title:  "Diving Into Lists"
type:   "Code"
slug:   "Amazing A+ List Practice"
upload: true
rubric: [ logic, clarity ]
---

We barely scratched the surface on loops in class, but spending time practicing them would be good. Where possible, these problems involve a for loop and possibly some additional variables to track what you're seeing as you loop through the data.

Remember, the pattern for a for loop is:

```python
for __element_name__ in __list_name__:
  __indented loop body 1__
  __indented loop body 2__

__stuff after the loop__
```

Or, as an example, this code prints the numbers 1-10.

```python
for num in range(1, 10):
  print(num)
```

# Problems

1. Define a list called **veggies**. It should contain 6-8 strings that are vegetables. Pick some of your favorites. For example, I might write
    ```python
    veggies = ["potatoes", "daikon"]
    ```

1. Define a list called **sandwichToppings**. It should be stuff that people put on sandwiches. For example, "peanut butter" is popular in the US, or some people like "ham", and others like "[vegemite](https://www.youtube.com/watch?v=XfR9iY5y94s)".

1. Define a list called **places**. This should be a list of strings that are places you have been, or might like to be. For example, I could define the list ["Lewiston", "Bloomington", "Berea", "Canterbury", "Boston"], as a starting point.

1. Define a function called **printList**. It should consume a list, loop through the list, and print each element. It should, after printing all of the elements of the list, return True. I've provided this as an example question:
 
    ```python
    # CONTRACT
    # printList : list -> boolean
    # PURPOSE
    # Prints all of the elements of a list.
    def printList (ls):
      for elem in ls:
        print(elem)
      return True

    # CHECKS
    # It is hard to check to see if things print the "right" things. So, we'll just 
    # run the function with multiple inputs and see if it prints the right things.
    printList(veggies)
    printList(sandwichToppings)
    ```

1. Define a function called **listLength**. It should consume a list, loop through it, and count the number of elements encountered along the way. To do this, you may need to declare a variable, called *count*, that is first initialized to zero. As you make your way through the list, you can increment it. After looping through the list, you should return your count. 

1. Define a function called **longestString**. This should consume a list of strings, and return the string that is the longest. In my veggie list, that would be "potatoes", for example.

1. Define a function called **shortestString**. This should consume a list of strings, and return the string that is the shortest.

1. Define a function called sumList. This should take a list of numbers, and return the sum of all of the numbers in the list. Some checks might look like:
    ```python
    print("The sum of [3, 3, 3] is " + str(sumList([3, 3, 3])))
    print("The sum of [1, 2, 3] is " + str(sumList([1, 2, 3])))
    ```

1. Define a function called **sumUpTo**. This should consume a number, and return the summation of all of the numbers from 1 to that number. For example, **sumUpTo(10)** should return 55. Hint: look up the range() function for how to generate a list of numbers. You could implement this using sumList, if you wanted to,


## Building Lists

Now, you should look up *append*. This is a function that operates *on* a list. So, it works like this:

```python
ls = []
ls.append("apples")
ls.append("oranges")
result = longestString(ls)
print("I think this will say oranges: " + result)
```

We'll talk more about what is going on here, in terms of syntax. The important thing you should know is that you can take a list, add a dot, and then say **append(stuff)**, and it will append stuff to the list on the left-hand side of the dot. 

You can use this to build new lists!

1. Define a function **onlyOdds** that takes a list of numbers, and returns only the odd numbers. (I'm giving this to you as an example.)
    ```python
    def onlyOdds (lon):
      templs = []
      for n in lon:
        if (n % 2) == 1:
          templs.append(n)
      return templs
      ```
1. Define a function called **oddStrings**. Given a list of strings, it should return a new list that contains only the strings of odd length.

1. Define a function called **betweenLetters**. This takes three arguments, a lower-bound letter, an upper-bound letter, and a list of strings. It should return only the strings that start with letters in the range you are given. For example, I might say **betweenLetters("a", "m", veggies)**, and I should get back the list ["daikon"]. Some hints: the function list() converts a string into a list of characters, and we talked about how to get the first thing from a list in class -- it involves indexing. Then, to turn a character into an umber, you can use the *ord()* function, which (for example) when given **ord("A")**, you would get back 65. This problem is trickier than the others, but that is only because there are more things you have to do, not because it represents a radical departure from previous problems.

1. Define a function called **findIn**. Given a string and a list of strings, return True if you can find the string in the list of strings. Return False if you cannot. 

## More Practice

Now, if you've made it this far, you might want to wander over to CodingBat and explore some of the list practice problems. The following sets of problems are not a bad place to look for inspiration...

* For logic, [Logic-1](http://codingbat.com/python/Logic-1) and [Logic-2](http://codingbat.com/python/Logic-1) are good.

* For lists, [List-1](http://codingbat.com/python/List-1) and [List-2](http://codingbat.com/python/List-2) are good.

I highly recommend you and a partner work on these, and just keep doing them. And doing them. And doing them. The goal here is to get used to defining functions, writing if statements, and writing for loops, so that when you have to start solving interesting problems, you aren't wondering how to use the building blocks. We are, in essence, building up some *automaticity* in our practice and understanding of programming.

## Sentiment Analysis

I may try and simplify my sentiment analysis problem. Or, we might tackle it in class together. We'll see. For now, I wanted to get these problems pushed out to you.


