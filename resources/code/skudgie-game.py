from skudgie import Skudgie
import random

class Game(object):
    players = None
    currentPlayer = 0
    
    def __init__(self, numPlayers):
        self.players = []
        for playerNumber in range(0, numPlayers):
            self.players.append(Skudgie("Player {0}".format(playerNumber)))
    
    # CONTRACT
    # Plays a turn. No return value.
    def playTurn(self):
        # Grab the current skudgie
        currentSkudgie = self.players[self.currentPlayer]
        # Roll the die
        die = random.randint(0,9)
        added = currentSkudgie.addPart(die)
        print ("{0} added a {1}".format(currentSkudgie.getName(), added))
        self.currentPlayer = (self.currentPlayer + 1) % len(self.players)

    # CONTRACT
    # Returns True if we're done, False if we're not.
    def checkWinner(self):
        for player in self.players:
            if player.isComplete():
                print(player.getName() + " wins!")
                return True
        return False

    
g = Game(4)
isDone = False

while not isDone:
    g.playTurn()
    isDone = g.checkWinner()
    



            

    
    
